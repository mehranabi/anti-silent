### Anti-Silent

This is a simple Android application which let you to control your mobile phone with SMS.

You turn on/off your mobile phone by sending a CODE as text message.

### Tech Stack
- Java
- Android

### Developer
S. Mehran Abghari - mehran.ab80@gmail.com

### License
MIT
