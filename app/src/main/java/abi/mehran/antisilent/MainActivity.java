package abi.mehran.antisilent;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static abi.mehran.antisilent.Utils.makeNormal;
import static abi.mehran.antisilent.Utils.makeSilent;
import static abi.mehran.antisilent.Utils.makeVibrate;

public class MainActivity extends AppCompatActivity {

    Button normalBtn, vibrateBtn, silentBtn;
    TextView statusTv;

    AudioManager audioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initAudioManager();
        updateStatus();
        askNotificationPermission();
        askPermissions();
    }

    private void askPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS}, 0);
        }
    }

    private void initViews() {
        normalBtn = findViewById(R.id.normal_btn);
        vibrateBtn = findViewById(R.id.vibrate_btn);
        silentBtn = findViewById(R.id.silent_btn);

        normalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeNormal(audioManager);
                updateStatus();
            }
        });

        vibrateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeVibrate(audioManager);
                updateStatus();
            }
        });

        silentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeSilent(audioManager);
                updateStatus();
            }
        });

        statusTv = findViewById(R.id.status_tv);
    }


    private void initAudioManager() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
    }

    private void updateStatus() {
        int ringerMode = this.audioManager.getRingerMode();
        String mode;

        switch (ringerMode) {
            case AudioManager.RINGER_MODE_NORMAL:
                mode = "NORMAL";
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                mode = "VIBRATE";
                break;
            case AudioManager.RINGER_MODE_SILENT:
                mode = "SILENT";
                break;
            default:
                mode = "Unknown";
                break;
        }

        statusTv.setText(mode);
    }

    private void askNotificationPermission() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !notificationManager.isNotificationPolicyAccessGranted()) {
            Intent intent = new Intent(android.provider.Settings.ACTION_NOTIFICATION_POLICY_ACCESS_SETTINGS);
            startActivity(intent);
        }
    }
}