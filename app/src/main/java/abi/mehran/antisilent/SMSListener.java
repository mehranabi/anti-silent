package abi.mehran.antisilent;

public interface SMSListener {
    void smsReceived(String message);
}
