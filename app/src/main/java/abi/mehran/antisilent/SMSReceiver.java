package abi.mehran.antisilent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SMSReceiver extends BroadcastReceiver {
    private static SMSListener mListener;
    public static void bindListener(SMSListener listener) {
        mListener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data = intent.getExtras();
        Object[] pdus = (Object[]) data.get("pdus");
        for (Object o : pdus) {
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) o);
            String message = smsMessage.getMessageBody();

            ProcessMessage.start(context, message);

            if (mListener != null) {
                mListener.smsReceived(message);
            }
        }
    }
}
