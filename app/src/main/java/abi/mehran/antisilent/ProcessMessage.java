package abi.mehran.antisilent;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.media.AudioManager;

import static abi.mehran.antisilent.Utils.MAKE_NORMAL;
import static abi.mehran.antisilent.Utils.MAKE_SILENT;
import static abi.mehran.antisilent.Utils.MAKE_VIBRATE;

public class ProcessMessage extends IntentService {
    public ProcessMessage() {
        super("ProcessMessage");
    }

    public static void start(Context context, String message) {
        Intent intent = new Intent(context, ProcessMessage.class);
        intent.putExtra("MESSAGE", message);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String message = intent.getStringExtra("MESSAGE");
            if (message != null) {
                AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

                switch (message) {
                    case MAKE_NORMAL:
                        Utils.makeNormal(audioManager);
                        break;
                    case MAKE_VIBRATE:
                        Utils.makeVibrate(audioManager);
                        break;
                    case MAKE_SILENT:
                        Utils.makeSilent(audioManager);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
