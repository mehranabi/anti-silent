package abi.mehran.antisilent;

import android.media.AudioManager;

public class Utils {
    public static final String MAKE_NORMAL = "**NORMAL**";
    public static final String MAKE_VIBRATE = "**VIBRATE**";
    public static final String MAKE_SILENT = "**SILENT**";

    public static void makeNormal(AudioManager manager) {
        manager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
    }
    public static void makeVibrate(AudioManager manager) {
        manager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
    }
    public static void makeSilent(AudioManager manager) {
        manager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
    }
}
